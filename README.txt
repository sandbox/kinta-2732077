Gnusocial drupal module
-----------------------
Provides a field to enable gnusocial comments on any content type. It depends 
on composer manager to enable the needed libraries (oauth-subscriber) through 
composer.

You can use this module to post statuses when a new content is published, or
simply to attach any conversation living in a gnusocial instance that implements
the statusnet api to retrieve the conversation.

If you want to simply attach the conversation
---------------------------------------------
You'll have to add the field to the content you want in field configuration
check  "Gnusocial Comments" and uncheck "Post to GnuSocial". For every post
you'll have to manually fill the field with the conversation json url. It's your
job to get it.

To get full functionality (attach comments and post to gnusocial):
------------------------------------------------------------------
You'll need to configure both in gnusocial and drupal.
For Gnusocial part create the user that will act as bot. It will create the 
statuses anouncing your blog entries, allowing to create a conversation over.  
Go to: Settings >connections; There click on register an Oauth client applic-
ation in right sidebar (https://gnusocialurl/settings/oauthapps).

You'll have to register a new application, so fill the fields:
- Icon if you want it,
- Name: a descriptive name of the app, for example drupal_gnusocial.
- Description: Explain this application, for example: Communicates with a 
  drupal site.
- Source Url: The drupal url.
- Organization: Optionally, the name of the organization that owns the drupal 
  with gnusocial integration.
- Homepage: The homepage of the organization .
- Callback url: http://yourdrupal/admin/config/services/gnusocial.
- Type of application: Browser.
- Read-write

After that a "Consumer key" and "Consumer secret" will be provided. Copy these
in a text editor. Don't close the gnusocial tab (session) yet.

We'll go to your drupal instance, there configure in: configuration > services >
gnusocial. It will do a 3-legged authentication against the provided gnusocial
url, so add the base url of gnusocial ( https://yourgnusocial.url.com ), OAuth
consumer key, the "Consumer secret" and click save. After saving, click "update
tokens", it will redirect to gnusocial url to complete the authentication,
allowing the application to interact with gnusocial, after that it will redirect
to drupal and the process will be completed.


Developer info:
---------------
You can use templates to theme the comments:
- field: You can use the field template as gnusocial comments is a field.
- gnusocial-conversation.html.twig will be the container of each conversation 
  (between field wrapper and each status). It has the statuses list, the more 
  link, and the conversation link.
- gnusocial-status.html.twig will be the template used to render each status.


It remains as a BIG TODO the plan to allow commenting directly from drupal.
