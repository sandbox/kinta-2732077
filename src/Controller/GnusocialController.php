<?php


namespace Drupal\gnusocial\Controller;

use Drupal\gnusocial\GnusocialService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\ClientFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for gnusocial module routes.
 */
class GnusocialController extends ControllerBase {

  /**
   * The Gnusocial service.
   *
   * @var \Drupal\gnusocial\GnusocialService
   */
  protected $gnusocialService;
  /**
   * Guzzle Http Client Factory.
   *
   * @var GuzzleHttp\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    GnusocialService $gnusocial_service,
    ClientFactory $http_client_factory
  ) {
    $this->gnusocialService = $gnusocial_service;
    $this->httpClientFactory = $http_client_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('gnusocial.manager'),
      $container->get('http_client_factory')
    );
  }

}
