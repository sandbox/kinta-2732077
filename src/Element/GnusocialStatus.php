<?php

namespace Drupal\gnusocial\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\gnusocial\GnusocialService;

/**
 * Provides GnusocialStatus render element.
 *
 * @RenderElement("gnusocial_status")
 */
class GnusocialStatus extends RenderElement implements ContainerFactoryPluginInterface {

  /**
   * The Gnusocial service.
   *
   * @var \Drupal\gnusocial\GnusocialService
   */
  protected $gnusocialService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GnusocialService $gnusocial_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->gnusocialService = $gnusocial_service;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\gnusocial\GnusocialService $gnusocial_service */
    $gnusocial_service = $container->get('gnusocial.manager');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $gnusocial_service
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return array(
      '#title' => '',
      '#url' => '',
      '#identifier' => '',
      '#callbacks' => '',
      '#theme' => 'gnusocial_status',
      '#attributes' => ['id' => 'gnusocial_conversation'],
      '#pre_render' => [
       [get_class($this), 'preRenderGnusocialStatus'],
      ],
    );
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderGnusocialStatus(array $element) {

    // Create a statusnet_html render array using #statusnet_html.
    $element['statusnet_html'] = [
      '#markup' => $element['#statusnet_html'],
    ];
    // Create a date render array using #created_at.
    $element['created_at'] = [
      '#markup' => $element['#created_at'],
    ];
    // Avatar.
    $element['user_profile_image_url'] = [
      '#markup' => $element['#user_profile_image_url'],
    ];
    // User url.
    $element['user_url'] = [
      '#markup' => $element['#user_url'],
    ];
    // User statusnet profile url.
    $element['user_statusnet_profile_url'] = [
      '#type' => 'link',
      '#title' => $element['#user_screen_name'],
      '#url' => Url::fromUri($element['#user_statusnet_profile_url']),
    ];
    // Screen name - wrapped with url.
    $element['username'] = [
      '#type' => 'link',
      '#title' => $element['#user_screen_name'],
      '#url' => Url::fromUri($element['#user_url']),
    ];

    // Status url.
    $element['status_url'] = [
      '#type' => 'link',
      '#title' => $element['#url'],
      '#url' => Url::fromUri($element['#url']),
    ];


    $username = drupal_render($element['user_statusnet_profile_url']);
    $created_at = drupal_render($element['created_at']);

    // Submitted note as drupal does.
    $element['submitted'] = [
      '#markup' => t('Submitted by @username on @datetime', array('@username' => $username, '@datetime' => $created_at)),
    ];

    // Permalink.
    $element['permalink'] = [
      '#type' => 'link',
      '#title' => 'permalink',
      '#url' => Url::fromRoute('<current>', [], ["fragment" => "gs-comment-" . $element['#id']]),
    ];

    return $element;
  }

}
