<?php

namespace Drupal\gnusocial\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\gnusocial\GnusocialService;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides GnuSocial JS render element.
 *
 * @RenderElement("gnusocial_js")
 */
class GnusocialJS extends RenderElement implements ContainerFactoryPluginInterface {

  /**
   * The Gnusocial service.
   *
   * @var \Drupal\gnusocial\GnusocialService $gnusocialService
   */
  protected $gnusocialService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GnusocialService $gnusocial_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->gnusocialService = $gnusocial_service;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\gnusocial\GnusocialService $gnusocial_service */
    $gnusocial_service = $container->get('gnusocial.manager');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $gnusocial_service
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return array(
      '#title' => '',
      '#url' => '',
      '#identifier' => '',
      '#callbacks' => '',
      '#theme_wrappers' => ['gnusocial_noscript', 'container'],
      '#attributes' => ['id' => 'gnusocial_conversation'],
      '#pre_render' => [
        get_class() . '::generatePlaceholder',
      ],
    );
  }

  /**
   * The #pre_render callback to generate a placeholder.
   *
   * @param array $element
   *   A renderable array.
   *
   * @return array
   *   The updated renderable array containing the placeholder.
   */
  public static function generatePlaceholder(array $element) {
    if (\Drupal::currentUser()->hasPermission('view gnusocial comments')) {
      $element[] = [
        '#lazy_builder' => [
          get_class() . '::displayGnusocialComments',
          [
            $element['#title'],
            $element['#url'],
            $element['#identifier'],
          ],
        ],
        '#create_placeholder' => TRUE,
      ];
    }

    return $element;
  }

  /**
   * Post render function to inject the GnuSocial JavaScript. TODO.
   */
  public static function displayGnusocialComments($title, $url, $identifier) {
    $gnusocial_settings = \Drupal::config('gnusocial.settings');
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    $gs = \Drupal::service('gnusocial.manager');
    $element = [
      '#theme_wrappers' => ['gnusocial_noscript', 'container'],
      '#attributes' => ['id' => 'gnusocial_conversation'],
    ];
    $renderer->addCacheableDependency($element, $gnusocial_settings);

    $stream = $gs->getData("/api/statuses/show/11.json");
    // $avatar_markup = "<a href='".$avatar->statusnet_profile_url."'>
    // <img src='".$avatar->profile_image_url."'>".$avatar->screen_name."</a>";.
    $gnusocial = [
      'domain' => $gnusocial_settings->get('gnusocial_url'),
      'url' => $url,
      'title' => $title,
      'identifier' => $identifier,
      'data' => ['#markup' => "<pre>" . $stream->text . "</pre>"],
    ];

    // If the user is logged in, we can inject the username and email.
    /*
    $account = \Drupal::currentUser();
    if ($gnusocial_settings->get('behavior.gnusocial_inherit_login') &&
    !$account->isAnonymous()) {
    $renderer->addCacheableDependency($element, $account);
    $gnusocial['name'] = $account->getUsername();
    $gnusocial['email'] = $account->getEmail();
    }
     */

    // Provide alternate language support if desired.
    /*
    if ($gnusocial_settings->get('behavior.gnusocial_localization')) {
    $gnusocial['language'] = \Drupal::languageManager()
    ->getCurrentLanguage()
    ->getId();
    }
     */

    // Check if we want to track new comments in some stats analytics service.
    /*
    if ($gnusocial_settings
    ->get('behavior.gnusocial_track_newcomment_stats')) {
    // Add a callback when a new comment is posted.
    $gnusocial['callbacks']['onNewComment'][] =
    'Drupal.disqus.disqusTrackNewComment';
    // Attach the js with the callback implementation.
    //TODO: add stats service query conditional
    if piwik: $gnusocial['#attached']['library'][] = 'gnusocial/piwik_stats';
    if ga: $gnusocial['#attached']['library'][] = 'gnusocial/ga_stats';
    }
     */

    /*
     * Pass callbacks on if needed. Callbacks array is two dimensional array
     * with callback type as key on first level and array of JS callbacks on the
     * second level.
     *
     * Example:
     * @code
     * $element = [
     *   '#type' => 'gnusocial',
     *   '#callbacks' = [
     *     'onNewComment' => [
     *       'myCallbackThatFiresOnCommentPost',
     *       'Drupal.mymodule.anotherCallbInsideDrupalObj',
     *     ],
     *   ],
     * ];
     * @endcode
     */
    if (!empty($element['#callbacks'])) {
      $gnusocial['callbacks'] = $element['#callbacks'];
    }

    // Add the gnusocial.js and all the settings to process the JavaScript and
    // load Gnusocial.
    $element['#attached']['library'][] = 'gnusocial/gnusocial.js';
    $element['#attached']['drupalSettings']['gnusocial'] = $gnusocial;
    return $element;
  }

}
