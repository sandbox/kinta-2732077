<?php

namespace Drupal\gnusocial\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\gnusocial\GnusocialService;

/**
 * Provides GnusocialConversation render element.
 *
 * @RenderElement("gnusocial_conversation")
 */
class GnusocialConversation extends RenderElement implements ContainerFactoryPluginInterface {

  /**
   * The Gnusocial service.
   *
   * @var \Drupal\gnusocial\GnusocialService
   */
  protected $gnusocialService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GnusocialService $gnusocial_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->gnusocialService = $gnusocial_service;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\gnusocial\GnusocialService $gnusocial_service */
    $gnusocial_service = $container->get('gnusocial.manager');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $gnusocial_service
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return array(
      '#title' => '',
      '#url' => '',
      '#identifier' => '',
      '#callbacks' => '',
      '#theme' => 'gnusocial_conversation',
      '#attributes' => ['id' => 'gnusocial_conversation'],
      '#pre_render' => [
       [get_class($this), 'preRenderGnusocialConversation'],
      ],
      '#cache' => ['max-age' => 0 ]
    );
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderGnusocialConversation(array $element) {

    // The statuses list.
    $element['statuses'] = $element['#statuses'];

    // The more link to json.
    $element['conversation_json'] = $element['#conversation_json'];

    // Url to first notice in conversation.
    $element['conversation_url'] = $element['#conversation_url'];

    return $element;
  }

}
