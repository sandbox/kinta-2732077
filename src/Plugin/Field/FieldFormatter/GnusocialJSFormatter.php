<?php
namespace Drupal\gnusocial\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default gnusocial_comments formatter obtained by the client via JS.TODO.
 *
 * @FieldFormatter(
 *   id = "gnusocial_js_comments",
 *   label = @Translation("[TODO]GnuSocial js comments"),
 *   field_types = {
 *     "gnusocial_comments"
 *   }
 * )
 */
class GnusocialJSFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, array());
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // As the Field API only applies the "field default value" to newly created
    // entities, we'll apply the default value for existing entities.
    if ($items->count() == 0) {
      $field_default_value = $items->getFieldDefinition()->getDefaultValue($items->getEntity());
      $items->status = $field_default_value[0]['status'];
    }

    if ($items->status == 1 && $this->currentUser->hasPermission('view gnusocial comments')) {
      $element[] = [
        '#type' => 'gnusocial_js',
        '#url' => $items->getEntity()->toUrl('canonical', ['absolute' => TRUE])->toString(),
        '#title' => (string) $items->getEntity()->label(),
        '#identifier' => $items->identifier ?: "{$items->getEntity()->getEntityTypeId()}/{$items->getEntity()->id()}",
      ];
    }

    return $element;
  }

}
