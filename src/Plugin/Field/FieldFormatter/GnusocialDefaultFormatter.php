<?php
namespace Drupal\gnusocial\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\gnusocial\GnusocialService;

/**
 * Provides a default gnusocial comment formatter.
 *
 * @FieldFormatter(
 *   id = "gnusocial_comments",
 *   label = @Translation("GnuSocial comments"),
 *   field_types = {
 *     "gnusocial_comments"
 *   }
 * )
 */
class GnusocialDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The gnusocial service.
   *
   * @var \Drupal\gnusocial\GnusocialService
   */
  protected $gnusocialService;

  /**
   * The gnusocial settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, AccountInterface $current_user, GnusocialService $gnusocial_service, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, array());
    $this->currentUser = $current_user;

    /** @var \Drupal\gnusocial\GnusocialService $gnusocial_service */
    $this->gnusocialService = $gnusocial_service;
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $container->get('current_user'),
      $container->get('gnusocial.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // As the Field API only applies the "field default value" to newly created
    // entities, we'll apply the default value for existing entities.
    if ($items->count() == 0) {
      $field_default_value = $items->getFieldDefinition()->getDefaultValue($items->getEntity());
      $items->status = $field_default_value[0]['status'];
    }
    foreach ($items as $delta => $item) {
      $elements[$delta] = array($this->viewItem($item));
    }
    /*
    // TODO: ADD LOGIN and remote comment form
    $container = [
    '#type' => 'container',
    '#children' => $elements
    ];
     */

    return $elements;
  }

  /**
   * View single gnusocial conversation .
   *
   * @var \Drupal\gnusocial\Plugin\Field\FieldType\GnusocialItem $item
   */
  public function viewItem($item) {
    $element = [];
    if ($item->status == 1 && $this->currentUser->hasPermission('view gnusocial comments')) {
      $config = $this->configFactory->get('gnusocial.settings');

      $stream = $this->gnusocialService->getData($item->conversation_id);
      if (is_null($stream)) {
        $element = ['#markup' => "No comments yet"];
      }
      else {
        $statuses = [
          // '#title' => $this->t("GnuSocial comments"),.
          '#theme' => 'item_list',
          '#attributes' => ['class' => ['gnusocial-conversation-wrapper']],
        ];
        foreach ($stream as $status) {
          $statuses['#items'][] = [
            '#type' => 'gnusocial_status',
            '#url' => $status->external_url,
            '#statusnet_html' => $status->statusnet_html,
            '#created_at' => $status->created_at,
            '#user_profile_image_url' => $status->user->profile_image_url,
            '#user_statusnet_profile_url' => $status->user->statusnet_profile_url,
            '#user_url' => (isset($status->user->url)) ? $status->user->url : $status->user->statusnet_profile_url,
            '#user_screen_name' => $status->user->screen_name,
            '#id' => $status->id,
          ];
        }

        // Reply info.
        $notice_url = Url::fromUri($stream[0]->external_url);

        $join_link = [
          '#type' => 'link',
          '#url' => $notice_url,
          '#title' => t('Join conversation in gnusocial'),
          '#attributes' => [
            'target' => "_blank",
            'class' => "gnusocial-conversation-link",
          ],
        ];
        // More link.
        $conversation_json = [
          '#type' => 'more_link',
          '#title' => t('json conversation'),
          '#attributes' => ['class' => "gnusocial-conversation-json-link"],
          '#url' => Url::fromUri($this->gnusocialService->getMoreLink($item->conversation_id)),
        ];
        $element['conversation'][] = [
          '#type' => 'gnusocial_conversation',
          '#statuses' => $statuses,
          '#conversation_url' => $join_link,
          '#conversation_json' => $conversation_json,
        ];
      }
    }
    return $element;
  }

}
