<?php
namespace Drupal\gnusocial\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'gnusocial_comments' field type.
 *
 * @FieldType(
 *   id = "gnusocial_comments",
 *   label = @Translation("GnuSocial comments"),
 *   description = @Translation("GnuSocial comments widget"),
 *   default_widget = "gnusocial_comments",
 *   default_formatter = "gnusocial_comments"
 * )
 */
class GnusocialItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'status' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 1,
        ],
        'conversation_id' => [
          'type' => 'varchar',
          'default' => '',
          'length' => 255,

        ],
        'post_gnusocial' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 1,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['status'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('GnuSocial status value'));
    $properties['conversation_id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('GnuSocial conversation identifier'));
    $properties['post_gnusocial'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Post to GnuSocial'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->get('status')->getValue() === '';
  }

}
