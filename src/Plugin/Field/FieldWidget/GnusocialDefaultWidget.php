<?php
namespace Drupal\gnusocial\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the Disqus comments widget.
 *
 * @FieldWidget(
 *   id = "gnusocial_comments",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "gnusocial_comments"
 *   }
 * )
 */
class GnusocialDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The gnusocial settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, AccountInterface $current_user, ConfigFactoryInterface $configFactory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, array());
    $this->currentUser = $current_user;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('gnusocial.settings');
    $is_new_entity = $items->getEntity()->isNew();
    $element['status'] = [
      '#type' => 'checkbox',
      '#title' => t('Gnusocial Comments'),
      '#description' => t('Users can post comments using <a href=":gnusocial">GnuSocial</a>.', [':gnusocial' => $config->get('gnusocial_url')]),
      '#default_value' => isset($items->status) ? $items->status : TRUE,
      '#access' => $this->currentUser->hasPermission('toggle gnusocial comments'),
    ];
    $element['post_gnusocial'] = [
      '#type' => 'checkbox',
      '#title' => t('Post to GnuSocial'),
      '#description' => t('Post to configured <a href=":gnusocial">GnuSocial</a> and automatically obtain the conversation identifier.', [':gnusocial' => $config->get('gnusocial_url')]),
      // DO NOT mark by default when is set (as it means updating)
      '#default_value' => $is_new_entity ? TRUE : FALSE,
      '#access' => $this->currentUser->hasPermission('post to gnusocial'),
    ];
    $element['conversation_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GnuSocial conversation identifier'),
      '#description' => $this->t("Unique identifier of the GnuSocial conversation thread. It's pulled from gnusocial automatically. You can use both local and remote urls. <br/>For gnusocial configured in settings example: <i>/api/statusnet/conversation/<b>first_status_id</b>.json</i>. <br/>Remote url example: <i>https://gnusocial.org/api/statusnet/conversation/<b>first_status_id</b>.json</i>. <br/> Changing this might cause comments to disappear. Use extreme caution!"),
      '#default_value' => isset($items->conversation_id) ? $items->conversation_id : '',
      '#access' => $this->currentUser->hasPermission('administer gnusocial'),
    ];

    // If the advanced settings tabs-set is available (normally rendered in the
    // second column on wide-resolutions), place the field as a details element
    // in this tab-set.
    if (isset($form['advanced'])) {
      $element += array(
        '#type' => 'details',
        '#group' => 'advanced',
      );
    }

    return $element;
  }

}
