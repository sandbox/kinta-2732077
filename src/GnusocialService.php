<?php
namespace Drupal\gnusocial;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Url;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

/**
 * Processes the communications with gnusocial.
 */
class GnusocialService {
  /**
   * Guzzle Http Client Factory.
   *
   * @var GuzzleHttp\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * The gnusocial settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs the GnusocialService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The Http client factory service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactoryInterface $config_factory,
    ClientFactory $http_client_factory
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->httpClientFactory = $http_client_factory;
    $this->configFactory = $config_factory;
  }

  /**
   * Return the tokens from consumer key and secret.
   *
   * If $authVerifier is nonexistant means that it is the first leg:
   * 1st leg: [oauth/request_token] Get temp tokens from oauth/request_token
   * from consumer keys:
   *            -we can detect this leg because no $authverifier parameter is
   *             provided.
   *            -link to authorize is provided.
   *             (https://url/api/oauth/authorize?oauth_token=temptokenxxxxx)
   * 2nd leg: [oauth/authorize] Login in gnusocial using previous link and
   * accepting the authorization request.
   *            -Once accepted we will get back to callback url (Drupal site)
   *             to continue to 3rd leg with authverifier.
   *
   * 3rd leg: [oauth/access_token] Getting manually authverifier from previous
   * url, it gets permanent tokens giving temp token and the authverifier.
   *            -we can detect this leg because authverifier parameter is
   *             provided.
   *
   * @param bool|string $authVerifier
   *   The verifier token or false if empty.
   */
  public function fetchTokens($authVerifier = FALSE) {
    $config = $this->configFactory->get('gnusocial.settings');

    $stack = HandlerStack::create();
    $middleware = new Oauth1([
      'consumer_key'  => $config->get('authentication.gnusocial_consumer_key'),
      'consumer_secret' => $config->get('authentication.gnusocial_consumer_secret'),
    // FALSE Means that 2-legged OAuth is needed.
      'token_secret' => ($authVerifier) ? $config->get('authentication.gnusocial_token_secret_tmp') : FALSE  ,
      'token' => ($authVerifier) ? $config->get('authentication.gnusocial_token_tmp') : NULL  ,
      'signature_method' => Oauth1::SIGNATURE_METHOD_HMAC,
    ]);
    $stack->push($middleware);
    /** @var Client $client */
    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => $config->get('gnusocial_url') . '/api/',
      'handler' => $stack,
      'auth' => 'oauth',
      // 'debug' => TRUE,.
    ]);
    $callback = Url::fromRoute('gnusocial.settings', [], array('absolute' => TRUE));
    /** @var Response $res */
    $res = ($authVerifier) ? $client->post('oauth/access_token', ['form_params' => ['oauth_verifier' => $authVerifier]]) : $client->post('oauth/request_token', ['form_params' => ['oauth_callback' => $callback->toString()]]);
    $params = (string) $res->getBody();
    $tokens = array();
    parse_str($params, $tokens);
    return $tokens;
  }

  /**
   * Get Data from GnuSocial api endpoint in json.
   *
   * @param string $resource
   *   The resource to get.
   */
  public function getData($resource) {
    $config = $this->configFactory->get('gnusocial.settings');

    $stack = HandlerStack::create();
    $middleware = new Oauth1([
      'consumer_key'  => $config->get('authentication.gnusocial_consumer_key'),
      'consumer_secret' => $config->get('authentication.gnusocial_consumer_secret'),
      'token_secret' => $config->get('authentication.gnusocial_token_secret') ,
      'token' => $config->get('authentication.gnusocial_token') ,
      'signature_method' => Oauth1::SIGNATURE_METHOD_HMAC,
    ]);
    $stack->push($middleware);

    /** @var bool $is_external */
    /*If url is external and so not configured via settings. */
    $is_external = (substr($resource, 0, 7) === "http://" or substr($resource, 0, 8) === "https://");
    /** @var string $base_uri */
    /*If different uri has been configured use that. */
    $base_uri = ($is_external) ? explode("/api/", $resource)[0] : $config->get('gnusocial_url');
    /** @var string $resource */
    /*If different uri has been configured use that. */
    $resource = ($is_external) ? "/api/" . explode("/api/", $resource)[1] : $resource;
    /** @var Client $client */
    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => $base_uri,
      'handler' => ($is_external) ? NULL : $stack,
      'auth' => ($is_external) ? FALSE : 'oauth',
    // 'debug' => TRUE,.
    ]);

    /** @var Response $data */
    $data = $client->get($resource);
    return json_decode($data->getBody());
  }

  /**
   * Get more link from resource.
   *
   * @param string $resource
   *   The resource to get. The same as getData but without auth.
   */
  public function getMoreLink($resource) {
    $config = $this->configFactory->get('gnusocial.settings');
    $base_uri = (substr($resource, 0, 7) === "http://" or substr($resource, 0, 8) === "https://") ? explode("/api/", $resource)[0] . "/api/" : $config->get('gnusocial_url');
    return $base_uri . $resource;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields($entity_type_id) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!$entity_type->isSubclassOf('\Drupal\Core\Entity\ContentEntityInterface')) {
      return [];
    }

    $map = $this->getAllFields();
    return isset($map[$entity_type_id]) ? $map[$entity_type_id] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAllFields() {
    $map = $this->entityFieldManager->getFieldMap();
    // Build a list of gnusocial comment fields only.
    $gnusocial_comment_fields = [];
    foreach ($map as $entity_type => $data) {
      foreach ($data as $field_name => $field_info) {
        if ($field_info['type'] == 'gnusocial_comments') {
          $gnusocial_comment_fields[$entity_type][$field_name] = $field_info;
        }
      }
    }
    return $gnusocial_comment_fields;
  }

  /**
   * Publishes a notice in gnusocial.
   */
  public function postNotice($status) {
    $config = $this->configFactory->get('gnusocial.settings');

    $stack = HandlerStack::create();
    $middleware = new Oauth1([
      'consumer_key'  => $config->get('authentication.gnusocial_consumer_key'),
      'consumer_secret' => $config->get('authentication.gnusocial_consumer_secret'),
      'token_secret' => $config->get('authentication.gnusocial_token_secret') ,
      'token' => $config->get('authentication.gnusocial_token') ,
      'signature_method' => Oauth1::SIGNATURE_METHOD_HMAC,
    ]);
    $stack->push($middleware);
    /** @var Client $client */
    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => $config->get('gnusocial_url'),
      'handler' => $stack,
      'auth' => 'oauth',
      'debug' => TRUE,
    ]);

    /** @var Response $res */
    $res = $client->post('/api/statuses/update.json', ['form_params' => ['status' => $status]]);
    $params = json_decode((string) $res->getBody());
    $answer = array($params);
    return $answer;
  }

}
