<?php

namespace Drupal\gnusocial\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\gnusocial\GnusocialService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Configure gnusocial settings for this site.
 */
class GnusocialSettingsForm extends ConfigFormBase {

  /**
   * Guzzle Http Client Factory.
   *
   * @var \Drupal\gnusocial\GnusocialService
   */
  protected $gnusocialService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    GnusocialService $gnusocial_service
  ) {
    $this->gnusocialService = $gnusocial_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('gnusocial.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gnusocial_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gnusocial.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gnusocial.settings');

    /* third leg of oauth process */
    $this->fillWithPermanentTokens();

    $form['gnusocial_url'] = array(
      '#type' => 'url',
      '#title' => t('GnuSocial url'),
      '#description' => $this->t('The gnusocial url ( https://example.gnusocial.com ).'),
      '#size' => 30,
      '#default_value' => $config->get('gnusocial_url'),
    );

    $form['settings'] = [
      '#type' => 'vertical_tabs',
      '#weight' => 50,
    ];

    // Behavior settings.
    $form['authentication'] = [
      '#type' => 'details',
      '#title' => $this->t('Authentication'),
      '#group' => 'settings',
    ];

    $form['authentication']['gnusocial_consumer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth consumer key'),
      '#default_value' => $config->get('authentication.gnusocial_consumer_key'),
    ];
    $form['authentication']['gnusocial_consumer_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth consumer secret'),
      '#default_value' => $config->get('authentication.gnusocial_consumer_secret'),
    ];
    $form['authentication']['gnusocial_token_tmp'] = [
      '#type' => 'hidden',
      '#title' => $this->t('OAuth temporal token'),
      '#default_value' => $config->get('authentication.gnusocial_token_tmp'),
      '#disabled' => TRUE,
    ];
    $form['authentication']['gnusocial_token_secret_tmp'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Oauth temporal secret token'),
      '#default_value' => $config->get('authentication.gnusocial_token_secret_tmp'),
      '#disabled' => TRUE,
    ];
    $form['authentication']['gnusocial_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth token'),
      '#default_value' => $config->get('authentication.gnusocial_token'),
      '#disabled' => TRUE,
    ];
    $form['authentication']['gnusocial_token_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Oauth secret token'),
      '#default_value' => $config->get('authentication.gnusocial_token_secret'),
      '#disabled' => TRUE,
    ];
    $form['authentication']['avatar'] = [
      '#type' => 'item',
      '#markup' => (NULL !== $config->get('authentication.gnusocial_token') ? $this->getAvatar() : $this->t('No Oauth process has been done')) ,
      '#title' => $this->t("The user that will be used to login to GnuSocial"),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['update_tokens'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update tokens'),
      '#submit' => array('::updateTokens'),
      '#disabled' => $config->get('authentication.gnusocial_consumer_secret') == "" && $config->get('authentication.gnusocial_consumer_key') == "",
    ];
    $form['actions']['verify_credentials'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verify credentials'),
      '#submit' => array('::verifyCredentials'),
      '#disabled' => $config->get('authentication.gnusocial_token') == "" && $config->get('authentication.gnusocial_token_secret') == "",
    ];

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit callback of update tokens button.
   */
  public function updateTokens(array &$form, FormStateInterface $form_state) {
    // Handle submitted values in $form_state here.
    $config = $this->config('gnusocial.settings');
    $consumer_key = $form_state->getValue('gnusocial_consumer_key');
    $consumer_secret = $form_state->getValue('gnusocial_consumer_key');

    $tokens = $this->gnusocialService->fetchTokens();

    if (array_key_exists('oauth_token', $tokens) && array_key_exists('oauth_token_secret', $tokens)) {
      $config
        ->set('authentication.gnusocial_token_tmp', $tokens['oauth_token'])
        ->set('authentication.gnusocial_token_secret_tmp', $tokens['oauth_token_secret'])
        ->save();

      // Oauth 1st leg: Temporal authorization succesful from gnusocial instance
      // at @url .', array('@url' => $form_state->getValue('gnusocial_url'))));
      // drupal_set_message($this->t("Oauth 2nd leg: To carry on, go to @url.",
      // array('@url' =>$form_state->getValue('gnusocial_url').
      // "/api/oauth/authorize?oauth_token=".$tokens['oauth_token']) ));.
      $goto_auth = new RedirectResponse($form_state->getValue('gnusocial_url') . "/api/oauth/authorize?oauth_token=" . $tokens['oauth_token']);
      $goto_auth->send();
    }
  }

  /**
   * Submit callback of update tokens button.
   */
  public function verifyCredentials(array &$form, FormStateInterface $form_state) {

    $this->gnusocialService->getData("/api/account/verify_credentials.json");
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('gnusocial.settings');

    $config
      ->set('gnusocial_url', $form_state->getValue('gnusocial_url'))
      ->set('authentication.gnusocial_consumer_key', $form_state->getValue('gnusocial_consumer_key'))
      ->set('authentication.gnusocial_consumer_secret', $form_state->getValue('gnusocial_consumer_secret'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Fills token items with permanent (Executes the 3rd leg of Oauth)
   */
  protected function fillWithPermanentTokens() {
    $config = $this->config('gnusocial.settings');
    if (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) {

      if ($_GET['oauth_token'] == $config->get('authentication.gnusocial_token_tmp') && $_GET['oauth_verifier']) {
        drupal_set_message($this->t("3rd leg of OAUTH coming back as callback from gnu social authorization giving us permanent tokens."));
        $tokens = $this->gnusocialService->fetchTokens($_GET['oauth_verifier']);
        if (array_key_exists('oauth_token', $tokens) && array_key_exists('oauth_token_secret', $tokens)) {
          $config
            ->set('authentication.gnusocial_token', $tokens['oauth_token'])
            ->set('authentication.gnusocial_token_secret', $tokens['oauth_token_secret'])
            ->save();
          drupal_set_message($this->t('Permanent authorization succesful from gnusocial instance at @url .', array('@url' => $config->get('gnusocial_url'))));

          $this->gnusocialService->getData("/api/account/verify_credentials.json");

        }

      }
    }
  }

  /**
   * Gets info about the user that will be used to comment and fetch notices.
   */
  protected function getAvatar() {
    $avatar = $this->gnusocialService->getData("/api/account/verify_credentials.json");
    $avatar_markup = "<a href='" . $avatar->statusnet_profile_url . "'><img src='" . $avatar->profile_image_url . "'>" . $avatar->screen_name . "</a>";
    return $avatar_markup;
  }

}
