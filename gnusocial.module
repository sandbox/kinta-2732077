<?php

/**
 * @file
 * Enables gnusocial conversation field.
 */

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Implements hook_theme().
 */
function gnusocial_theme() {
  $domain = \Drupal::config('gnusocial.settings')->get('gnusocial_url');
  return [
    'gnusocial_noscript' => [
      'variables' => [
        'message' => t('View the gnusocial conversation.'),
        'url' => $domain,
      ],
    ],
    'gnusocial_status' => [
      /*'variables' => [
        'url' => 'url',
      ],*/
      'render element' => 'element',
    ],
    'gnusocial_conversation' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node_form().
 *
 * Adds the book form element to the node form.
 *
 * @see gnusocial_node_form_submit()
 */
function gnusocial_form_node_form_alter(&$form, &$form_state, $form_id) {
  $form['actions']['publish']['#submit'][] = 'gnusocial_node_form_submit';
}

/**
 * Form submission handler for node_form().
 *
 * This handler is run for all nodes, checks if there is the gnusocial comments
 * field. If it is there, then it checks if post is checked, and status is true,
 * if all ok, then it will post the notice to gnusocial and will get the
 * conversation id to fill back the conversation_id option in gnusocial comments
 * field.
 * It's done this way because with insert/presave hook we can't obtain url yet,
 * and with update hook we cannot save consistent data because transaction is
 * going on.
 */
function gnusocial_node_form_submit(&$form, &$form_state) {
  $entity = $form_state->getFormObject()->getEntity();

  // Ensure only act on content entities.
  if (!($entity instanceof ContentEntityInterface)) {
    return;
  }

  // Ask if entity has gnusocial comments field.
  $field = \Drupal::service('gnusocial.manager')
    ->getFields($entity->getEntityTypeId());
  if (!$entity->hasField(key($field))) {
    return;
  }
  $field_name = key($field);
  $config = \Drupal::config('gnusocial.settings');
  // To get site name.
  $siteconfig = \Drupal::config('system.site');

  // Post to gnusocial if checked.
  // TODO allow to play with more than one gsfield.
  if ($form_state->getValue($field_name)[0]['post_gnusocial'] &&
      $form_state->getValue($field_name)[0]['status']) {
    $url = $entity->toUrl()->setAbsolute(TRUE);
    \Drupal::logger('gnusocial')->notice("callback bad bad " . $url->toString());

    // Get the rendered teaser display to include in status//TODO!!!
    $view_builder = \Drupal::entityManager()->getViewBuilder('node');
    $view_output = $view_builder->view($entity, 'teaser');
    $vb = drupal_render($view_output);
    $body = $entity->body->view(["settings" => ["format" => "plain_text"]]);
    $body_out = drupal_render($body);

    // Post to gnusocial. TODO add as render element to allow theming.
    $poster = \Drupal::service('gnusocial.manager')
      ->postNotice((string) t("[@sitename][post]\"@title\":\n @url \nSubmitted by @user (@userurl)", [
        "@sitename" => $siteconfig->get('name'),
        "@title" => $entity->getTitle(),
        "@url" => $url->toString(),
        "@user" => $entity->getOwner()->getDisplayName(),
        "@userurl" => $entity->getOwner()->toUrl()->setAbsolute(TRUE)->toString(),
      ]));

    $view_builder = \Drupal::entityManager()->getViewBuilder('node');
    // Fill the conversation_id field.
    $gs_field = $entity->get($field_name);
    $gs_field[0]->get('conversation_id')->setValue('/api/statusnet/conversation/' . $poster[0]->statusnet_conversation_id . ".json");
    $gs_field[0]->conversation_id = '/api/statusnet/conversation/' . $poster[0]->statusnet_conversation_id . ".json";
    $entity->save();
  }
}
